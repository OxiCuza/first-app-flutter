import 'package:flutter/material.dart';

class Result extends StatelessWidget {
  final int totalScore;
  final Function resetHandler;

  Result(this.totalScore, this.resetHandler);

  String get resultPhrase {
    String resultText;

    if (totalScore <= 8) {
      resultText = 'You are so awesome ! \nscore : $totalScore';
    } else if (totalScore <= 12) {
      resultText = 'Pretty likeable ! \nscore : $totalScore';
    } else if (totalScore <= 16) {
      resultText = 'You are strange ! \nscore : $totalScore';
    } else {
      resultText = 'You are bad ! \nscore : $totalScore';
    }

    return resultText;
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(
              resultPhrase,
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
              textAlign: TextAlign.center,
            ),
          ),
          FlatButton(
            child: Text('Restart Quiz !'),
            onPressed: () => resetHandler(),
            textColor: Colors.red[600],
            hoverColor: Colors.black,
          ),
        ],
      ),
    );
  }
}
