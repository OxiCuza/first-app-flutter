import 'package:flutter/material.dart';

import './question.dart';
import './answer.dart';

class Quiz extends StatelessWidget {
  final Function answerQuestions;
  final int index;
  final List<Map<String, Object>> collections;

  Quiz({@required this.collections, @required this.index, @required this.answerQuestions});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Question(
          collections[index]['questions'],
        ),
        ...(collections[index]['answers'] as List<Map<String, Object>>).map((answer) {
          return Answer(() => answerQuestions(answer['score']), answer['text']);
        }).toList()
      ],
    );
  }
}
