import 'package:flutter/material.dart';
import 'package:my_app/quiz.dart';
import 'package:my_app/result.dart';

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  int _index = 0;
  int _totalScore = 0;

  void _answerQuestions(int score) {
    _totalScore += score;

    setState(() {
      _index += 1;
    });
    print('Tombol ditekan !');
  }

  void _resetQuiz(){
    setState(() {
     _totalScore = 0;
     _index = 0; 
    });
  }

  final _collections = const [
    {
      'questions': 'What\'s your favorite color?',
      'answers': [
        {'text': 'Black', 'score': 10},
        {'text': 'Red', 'score': 5},
        {'text': 'Green', 'score': 3},
        {'text': 'White', 'score': 1},
      ],
    },
    {
      'questions': 'What\'s your favorite animal?',
      'answers': [
        {'text': 'Rabbit', 'score': 3},
        {'text': 'Snake', 'score': 11},
        {'text': 'Elephant', 'score': 5},
        {'text': 'Lion', 'score': 9},
      ],
    },
    {
      'questions': 'Who\'s your favorite instructor?',
      'answers': [
        {'text': 'Max', 'score': 1},
        {'text': 'Max', 'score': 1},
        {'text': 'Max', 'score': 1},
        {'text': 'Max', 'score': 1},
      ],
    },
  ];

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('Header Aplikasi'),
          backgroundColor: Colors.deepOrange,
          centerTitle: false,
          actions: <Widget>[Icon(Icons.android, color: Colors.green[200]),],
        ),
        body: _index < _collections.length
            ? Quiz(
                collections: _collections,
                index: _index,
                answerQuestions: _answerQuestions,
              )
            : Result(_totalScore, _resetQuiz),
      ),
    );
  }
}

// void main(){
//    runApp(MyApp());
// }
void main() => runApp(MyApp());
